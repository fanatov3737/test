<?php
include_once __DIR__ . '/autoload.php';

defined('APP_PATH') || define('APP_PATH', realpath(dirname(__FILE__)) . '/');

$input = new \Core\Input();

$action = (string)$input->getValue('action');
$fileRead = (string)$input->getValue('file');
$fileWrite = (string)$input->getValue('fileWrite');

$fileService = new \Core\Service\FileService();
$fileService->initReadAndWriteFiles($fileRead, $fileWrite);

/** @var \Core\Service\Arithmetic\ArithmeticInterface[] $arithmeticServices */
$arithmeticServices = [
    new \Core\Service\Arithmetic\PlusService(),
    new \Core\Service\Arithmetic\MinusService(),
    new \Core\Service\Arithmetic\DivisionService(),
    new \Core\Service\Arithmetic\MultiplyService()
];
$arithmeticService = null;
foreach($arithmeticServices as $item) {
    if ($item->actionName() === $action) {
        $arithmeticService = $item;
    }
}

if (!$arithmeticService) {throw new \Core\Exception\ArithmeticServiceNotFoundException("ArithmeticService not found for action=>{$action}");}

$consoleLogger = new \Core\Service\Logger\ConsoleLog();
$fileLogger = new \Core\Service\Logger\FileLog();

(new \Core\Service\ParseService($fileService, $arithmeticService, $fileLogger))->execute();

