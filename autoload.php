<?php
spl_autoload_register(function($className) {
    $className = str_replace('\\', DIRECTORY_SEPARATOR, $className);
    $filePath = __DIR__ . '/' . $className . '.php';

    if (!file_exists($filePath)) {throw new \Exception("Class {$className} not found");}

    include_once $filePath;
});