<?php
namespace Core\Exception;
/**
 * Class InvalidFileFormatException
 *
 * @package Core\Exception
 */
class InvalidFileFormatException extends \Exception
{
}