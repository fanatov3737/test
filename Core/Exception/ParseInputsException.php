<?php
namespace Core\Exception;
/**
 * Class ParseInputsException
 *
 * @package Core\Exception
 */
class ParseInputsException extends \Exception
{
}