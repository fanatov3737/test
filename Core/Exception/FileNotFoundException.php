<?php
namespace Core\Exception;
/**
 * Class FileNotFoundException
 *
 * @package Core\Exception
 */
class FileNotFoundException extends \Exception
{
}