<?php
namespace Core\Exception;
/**
 * Class ArithmeticServiceNotFoundException
 *
 * @package Core\Exception
 */
class ArithmeticServiceNotFoundException extends \Exception
{
}