<?php
namespace Core;

use Core\Exception\ParseInputsException;
/**
 * Class Inputs
 */
class Input
{
    /**
     * @var array
     */
    private $tokens;
    /**
     * @var array
     */
    private $parsed;
    /**
     * @var array
     */
    private $values;
    /**
     * Input constructor.
     *
     * @throws \Exception
     */
    public function __construct()
    {
        $argv = $_SERVER['argv'];
        array_shift($argv);

        $this->tokens = $argv;

        $this->parse();
    }
    /**
     * @param string $action
     *
     * @return mixed
     */
    public function getValue(string $action)
    {
        return $this->values[$action];
    }
    /**
     * Parses a long option.
     *
     * @param string $token The current token
     */
    private function parseOption($token)
    {
        $name = substr($token, 2);

        if (false !== $pos = strpos($name, '=')) {
            if (0 === \strlen($value = substr($name, $pos + 1))) {
                // if no value after "=" then substr() returns "" since php7 only, false before
                // see http://php.net/manual/fr/migration70.incompatible.php#119151
                if (\PHP_VERSION_ID < 70000 && false === $value) {
                    $value = '';
                }
                array_unshift($this->parsed, $value);
            }
            $this->values[substr($name, 0, $pos)] = $value;

        }
    }
    /**
     * @throws \Exception
     */
    protected function parse()
    {
        $parseOptions = true;
        $this->parsed = $this->tokens;

        while (null !== $token = array_shift($this->parsed)) {
            if ($parseOptions && 0 === strpos($token, '--')) {
                $this->parseOption($token);
            } else {
                throw new ParseInputsException('Cant parse whit token');
            }
        }
    }
}