<?php
namespace Core\Service\Arithmetic;
/**
 * Class MultiplyService
 *
 * @package Core\Service
 */
class MultiplyService implements ArithmeticInterface
{
    /**
     * @return string
     */
    public function actionName(): string
    {
        return 'multiply';
    }
    /**
     * @param int $value1
     * @param int $value2
     *
     * @return float
     */
    public function getResult(int $value1, int $value2) : float
    {
        return $value1 * $value2;
    }
}