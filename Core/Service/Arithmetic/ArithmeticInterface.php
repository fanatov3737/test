<?php
namespace Core\Service\Arithmetic;
/**
 * Class Command
 *
 * @package Core\Command
 */
interface ArithmeticInterface
{
    /**
     * @return string
     */
    public function actionName():string;
    /**
     * @param int $value1
     * @param int $value2
     *
     * @return float
     */
    public function getResult(int $value1, int $value2):float;
}