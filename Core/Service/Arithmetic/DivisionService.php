<?php
namespace Core\Service\Arithmetic;
/**
 * Class DivisionService
 *
 * @package Core\Service
 */
class DivisionService implements ArithmeticInterface
{
    /**
     * @return string
     */
    public function actionName(): string
    {
        return 'division';
    }
    /**
     * @param int $value1
     * @param int $value2
     *
     * @return float
     * @throws \Exception
     */
    public function getResult(int $value1, int $value2) : float
    {
        if ($value2 === 0) {throw new \Exception("Division by zero| {$value1};{$value2}");}

        return $value1 / $value2;
    }
}