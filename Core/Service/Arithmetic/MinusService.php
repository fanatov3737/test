<?php
namespace Core\Service\Arithmetic;
/**
 * Class MinusService
 *
 * @package Core\Service
 */
class MinusService implements ArithmeticInterface
{
    /**
     * @return string
     */
    public function actionName(): string
    {
        return 'minus';
    }

    /**
     * @param int $value1
     * @param int $value2
     *
     * @return float
     */
    public function getResult(int $value1, int $value2) : float
    {
        return $value1 - $value2;
    }
}