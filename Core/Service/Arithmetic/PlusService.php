<?php
namespace Core\Service\Arithmetic;
/**
 * Class PlusService
 *
 * @package Core\Service
 */
class PlusService implements ArithmeticInterface
{
    /**
     * @return string
     */
    public function actionName(): string
    {
        return 'plus';
    }
    /**
     * @param int $value1
     * @param int $value2
     *
     * @return float
     */
    public function getResult(int $value1, int $value2) : float
    {
        return $value1 + $value2;
    }
}