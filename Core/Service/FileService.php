<?php
namespace Core\Service;

use Core\Exception\FileNotFoundException;
use Core\Exception\InvalidFileFormatException;
/**
 * Class FileService
 *
 * @package Core\Command
 */
class FileService
{
    /**
     * @var string
     */
    private $readFilePath;
    /**
     * @var string
     */
    private $writeFilePath;
    /**
     * @param $filePath
     *
     * @return bool
     */
    protected function isCSV(string $filePath) : bool
    {
        $csvMimeTypes = [
            'text/csv',
            'text/plain',
            'application/csv',
            'text/comma-separated-values',
            'application/excel',
            'application/vnd.ms-excel',
            'application/vnd.msexcel',
            'text/anytext',
            'inode/x-empty',
            'application/octet-stream',
            'application/txt',
        ];
        $finfo = finfo_open(FILEINFO_MIME_TYPE);
        $mimeType = finfo_file($finfo, $filePath);

        return in_array( $mimeType, $csvMimeTypes );
    }
    /**
     * @param string $fileName
     *
     * @return string
     * @throws \Exception
     */
    private function getFullPath(string $fileName)  : string
    {
        $filePath = APP_PATH . '/' . $fileName;

        return $filePath;
    }
    /**
     * @param string $fileName
     *
     * @return string
     * @throws \Exception
     */
    private function checkFile(string $filePath)
    {
        if (!file_exists($filePath)) {throw new FileNotFoundException('File not found');}
        if (!$this->isCSV($filePath)) {throw new InvalidFileFormatException('File must have csv format');}
    }
    /**
     * @param string $readFileName
     * @param string $writeFileName
     *
     * @throws \Exception
     */
    public function initReadAndWriteFiles(string $readFileName, string $writeFileName)
    {
        $this->readFilePath = $this->getFullPath($readFileName);
        $this->writeFilePath = $this->getFullPath($writeFileName);

        $this->checkFile($this->readFilePath);
        $this->checkFile($this->writeFilePath);
    }
    /**
     * @throws \Exception
     */
    public function getReadFileData()
    {
        return explode("\n", file_get_contents($this->readFilePath));
    }
    /**
     * @param array $data
     */
    public function write(array $data)
    {
        $fileData = implode("\n", $data);
        file_put_contents($this->writeFilePath, $fileData);
    }
}