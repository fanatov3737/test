<?php
namespace Core\Service\Logger;
/**
 * Class Command
 *
 * @package Core\Command
 */
interface LoggerInterface
{
    /**
     * @param $msg
     *
     * @return mixed
     */
    public function log(string $msg);
}