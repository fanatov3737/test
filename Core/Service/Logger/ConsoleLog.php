<?php
namespace Core\Service\Logger;
/**
 * Class ConsoleLog
 *
 * @package Core\Service
 */
class ConsoleLog implements LoggerInterface
{
    /**
     * @param $msg
     *
     * @return mixed
     */
    public function log(string $msg)
    {
        echo "{$msg}\n";
    }
}