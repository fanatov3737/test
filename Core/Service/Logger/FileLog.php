<?php
namespace Core\Service\Logger;
/**
 * Class FileLog
 *
 * @package Core\Service
 */
class FileLog implements LoggerInterface
{
    /**
     * @var string
     */
    private $filePath = APP_PATH . '/log.txt';
    /**
     * FileLog constructor.
     */
    public function __construct()
    {
        $this->clearFile();
    }
    /**
     *
     */
    public function clearFile()
    {
        file_put_contents($this->filePath, '');
    }
    /**
     * @param $msg
     *
     * @return mixed
     */
    public function log(string $msg)
    {
        file_put_contents(APP_PATH . '/log.txt', $msg.PHP_EOL , FILE_APPEND | LOCK_EX);
    }
}