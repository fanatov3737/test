<?php
namespace Core\Service;

use Core\Service\Arithmetic\ArithmeticInterface;
use Core\Service\Logger\LoggerInterface;
/**
 * Class PlusService
 *
 * @package Core\Service
 */
class ParseService
{
    /**
     * @var FileService
     */
    private $fileService;
    /**
     * @var ArithmeticInterface
     */
    private $arithmeticService;
    /**
     * @var LoggerInterface
     */
    private $logger;
    /**
     * Command constructor.
     *
     * @param FileService $fileService
     */
    public function __construct(FileService $fileService, ArithmeticInterface $arithmeticService, LoggerInterface $logger)
    {
        $this->fileService = $fileService;
        $this->arithmeticService = $arithmeticService;
        $this->logger = $logger;
    }
    /**
     * @param $input
     *
     * @throws \Exception
     */
    public function execute()
    {
            $fileData = $this->fileService->getReadFileData();
            $result = [];
            foreach ($fileData as $item) {
                try {
                    $itemExplode = explode(';', $item);
                    $value = $this->arithmeticService->getResult($itemExplode[0], $itemExplode[1]);
                    if ($value > 0) {
                        $result[] = "$item;$value";
                    } else {
                        $this->logger->log("wrong=>$item");
                    }

                } catch (\Exception $exception) {
                    $this->logger->log("Error=>{$exception->getMessage()}");
                }
            }
            $this->fileService->write($result);

    }
}